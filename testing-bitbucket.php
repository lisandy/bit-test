<?php
// Load config file
require_once( dirname( dirname( __FILE__ ) ) . '/inc/config.php' );

// Page Title
$page_title = new Page_Title( 'Title Here' );

// Body Class
$body_class = new Body_Class( $page_title->page_title );

// Set page-specific variables
$ua['title-tag']      = $page_title->get_page_title();
$ua['page-title']     = $page_title->page_title;
$ua['body-class']     = $body_class->get_body_class();
$ua['meta-desc']      = 'Code Testing';
//$ua['header-bg-top']  = 'primary';
//$ua['header-bg-main'] = 'transparent';

$ua['scripts'] = array(
	'countdown-js' => array(
		'src' => SITE_URL . '/commencement/assets/js/countdown.js',
		'ver' => '',
	)
);

// Include header
require_once( ABSPATH . '/commencement/inc/theme/header.php' );
?>

<div class="touch-overlay"></div>
 <div style="background-color: #000000;">
    <div class="bg-img bg-img-lg video-area" style="background-image: url( https://www.ua.edu/img/commencement02_1920.jpg ); background-color: #000000;">
    	<iframe id="bgvid" title="A background video, with no sound, playing slowed down scenes of people interacting at past Commencement ceremonies of The University of Alabama." class="mobile-hide" src="https://player.vimeo.com/video/261871446?background=1&loop=1&title=0&byline=0&portrait=0" style="margin-top:-135px;" width="1920" height="1080" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    </div> <!-- .bg-img -->

    <div class="site-content text-center commencement-page-header-box" id="skip-to-content">
		<span class="commencement-page-header-date">August 3, 2019</span>
        <h1 class="commencement-page-header">Commencement</h1>
	</div><!-- .site-content -->

    <div class="bg-transparent-primary countdown-margins">
	    <div class="container text-center">
        	<!--  <h2 class="margin-bottom-minus-20">you’ll be a #BamaGrad in</h2>--> 
	    	<h2 class="margin-bottom-minus-20">Congratulations, you're a <br /><a href="https://twitter.com/UofAlabama" class="bamagradlink">#BamaGrad</a></h2> 
	        <div class="row text-center">
	           <!-- <div id="demo" class="padding-top-15 countdown"></div> .demo -->
	    	</div> <!-- .row -->
			<div class="mobile-hide" aria-hidden="true">
				<div id="video-pause-button">  
					<div id="pause-button">
						<a href="#pause-button-shape" class="btn btn-transparent" ><span class="sr-only">Pause Button</span><i class="fa fa-pause fa-fw"></i></a>
					</div>
				</div>
				<div id="video-play-button">  
					<div id="play-button">
						<a href="#play-button-shape" class="btn btn-transparent" ><span class="sr-only">Play Button</span><i class="fa fa-play fa-fw"></i></a>
					</div>
				</div>
			</div> <!-- .row -->
    	</div><!-- .container -->
    </div><!-- .bg-transparent-primary  -->
</div>

    <section class="site-content site-content-box-clickable bg-dark">
    	<div class="container">
			<div class="row no-bottom-spacing no-top-spacing">
            	<div class="col-md-4">
					<div class="content-box text-center box-clickable  commencement-box-clickable">
						<i class="fa fa-graduation-cap icon-xl" style="color:#fff;"></i>
						<h3><a href="https://registrar.ua.edu/graduation/" style="color:#fff;">Apply to Graduate</a></h3>
					</div> <!-- .content-box -->
				</div> <!-- .col-md-4 -->

				<div class="col-md-4">
					<div class="content-box text-center box-clickable  commencement-box-clickable">
						<i class="fa fa-university icon-xl" style="color:#fff;"></i>
						<h3><a href="<?php echo SITE_URL; ?>/commencement/ceremony" style="color:#fff;">Ceremony Information</a></h3>
					</div> <!-- .content-box -->
				</div> <!-- .col-md-4 -->

				<div class="col-md-4">
					<div class="content-box text-center box-clickable  commencement-box-clickable">
						<i class="fa fa-car icon-xl" style="color:#fff;"></i>
						<h3><a href="<?php echo SITE_URL; ?>/commencement/travel" style="color:#fff;">Travel</a></h3>
					</div> <!-- .content-box -->
				</div> <!-- .col-md-4 -->
            </div> <!-- .row -->
		</div> <!-- .container -->
	</section>

    <!--  <div class="mini-note bg-primary">
    	<div class="container">
			<div class="row">
            	<div class="col-md-12">
                	<span class="commencment-notification">!</span> <p class="lead font-weight-500 padding-x-5 margin-left-60">All degree candidates must arrive at the Crisp Indoor Practice Facility 90 minutes prior to the start of their ceremony. #BamaGrad</p>
				</div>
            </div>
		</div>
	</div> .mini-note -->

<section class="site-content">
	<div class="container">
		<div class="row margin-top-20">
        	<div class="col-md-12">
				<h2 class="uppercase font-weight-600 padding-x-5"><i class="fa fa-tv fa-lg fa-fw"></i> Live Webcasts</h2>
                <p class="lead font-weight-500 padding-x-5">If you coudn't attend in person, you can still share this once-in-a-lifetime moment with your UA graduate.</p>
				
                <div class="row">
                    <div class="col-md-4 col-sm-6">
						<p class="margin-bottom-10"><strong class="uppercase">Saturday Ceremony:</strong><br>
                		<strong>Saturday, August 03, 9:00 a.m. CDT</strong><br>
               			Commencement ceremony for all graduate and undergraduate students was held in Coleman Coliseum.</p>
               			<p class="h4 no-top-spacing"><i class="fa fa-video-camera fa-fw"></i><strong> Live Video</strong> <small>(coming soon)</small></p>
					</div> <!-- .col-md-4 -->

                    <div class="col-md-4 col-sm-6 no-top-spacing margin-bottom-10">
						<ul style="list-style:none; margin-left:0; padding-left:0;">
							<lh class="uppercase"><strong>More Resources:</strong></lh>
							<li class="padding-top-5"><a href="https://www.ua.edu/news/2019/07/ua-to-hold-summer-commencement-exercise-aug-3/"><i class="fa fa-newspaper-o fa-fw margin-right-5"></i>News Release</a></li>
							<li class="padding-top-5"><a href="https://www.ua.edu/news/2019/08/summer-2019-graduates-announced-at-ua/"><i class="fa fa-users fa-fw margin-right-5"></i>Graduates Announcement</a> </li>
							<li class="padding-top-5"><a href="<?php echo SITE_URL; ?>/commencement/img/2019-Summer-Commencement-Guide.pdf"><i class="fa fa-file-pdf-o fa-fw margin-right-5"></i>Commencement Guide</a></li> 
							<li class="padding-top-5"><a href="<?php echo SITE_URL; ?>/commencement/img/2019-Summer-Commencement-Program.pdf"><i class="fa fa-file-pdf-o fa-fw margin-right-5"></i>Commencement Program</a></li>
							<li class="padding-top-5"><a href="<?php echo SITE_URL; ?>/commencement/ceremony"><i class="fa fa-info-circle fa-fw margin-right-5"></i>Ceremony Information</a></li>
							<li class="padding-top-5"><a href="<?php echo SITE_URL; ?>/commencement/diplomas-degrees"><i class="fa fa-certificate fa-fw"></i> Diplomas &amp; Degrees</a></a>
							<!-- <li class="padding-top-5"><a href="<?php// echo SITE_URL; ?>/commencement/app"><i class="fa fa-mobile fa-fw margin-right-5"></i>Get the Commencement App</a></li> -->
							<li class="padding-top-5"><a href="<?php echo SITE_URL; ?>/visitors"><i class="fa fa-map fa-fw margin-right-5"></i>More Information for Visitors</a></li>
						</ul>
                	</div> <!-- .col-md-4 -->

                    <!-- SUMMER HEAT ALERT for summer commencements --> 
                    <div class="col-md-4 no-top-spacing margin-bottom-10">
                        <div class="content-box text-center bg-light">
                         	<i class="fa fa-sun-o icon-bg icon-primary icon-lg circle"></i>
                            <h3>Heat Alert</h3>
                            <p class="lead">Please be aware it is extremely hot when walking across campus and prepare accordingly!</p>
                        </div>   <!-- .content-box -->
                    </div>  <!-- .col-md-4 -->
				
                   <!--  <div class="col-md-3 col-sm-6 no-top-spacing margin-bottom-10">
                        <div class="content-box text-center bg-light margin-top-0">
                                    <i class="fa fa-map icon-bg icon-primary icon-lg circle"></i>
                                    <h3><a href="http://bit.ly/2PBBDGC">Commencement Campus Map</a></h3>
                                    <p>Please be aware of the weather and allow yourself extra time for travel.</p>
                        </div>  
                    </div> -->
				
			</div> <!-- .row -->
		</div> <!-- .col-md-12 -->
	</div> <!-- .row -->
</section>

<div style="background-color: #000000;">
	<div class="need-bg need-bg-home-mobile">
		<div class="container">
			<div class="row">
				<div class="col-md-2 need-text bg-primary">
					<span class="padding-left-10 font-weight-600">I NEED...</span>
				</div> <!-- .col-md-1 -->
				<div class="col-md-10 need-nav">
					<nav title="Helpful Links Navigation" class="need-nav-main">
						<ul class="nav need-navbar-nav">
							<li><a href="<?php echo SITE_URL; ?>/commencement/travel"><i class="fa fa-car fa-fw"></i> Directions</a></li>
							<li><a href="http://visittuscaloosa.com/?fwp_stay_price=110%2C111%2C112%2C113" ><i class="fa fa-bed fa-fw"></i> Hotels</a></li>
							<li><a href="<?php echo SITE_URL; ?>/commencement/travel#accessibility" ><i class="fa fa-universal-access fa-fw"></i> Accessibility</a></li>
							<li><a href="http://bit.ly/2W5osSu"><i class="fa fa-map fa-fw"></i> Campus Map</a></li>
							<li class="longer-nav-item longer-nav-item-last"><a href="https://map.concept3d.com/?id=1222#!ce/0,31812?ct/31812,35689,32541,32244,31831,31830"><i class="fa fa-star fa-fw"></i> UA Points of Interest</a></li>
						</ul>
					</nav>
				</div> <!-- .col-md-11 -->
			</div> <!-- .row -->
		</div> <!-- .container -->
	</div> <!-- .need-bg -->
</div> <!-- accessibility black -->

<section class="site-content bg-center-bottom" style="background-image:url(img/map-cb-img.jpg); background-color: #000000;">
	<div class="container">
		<div class="row margin-top-20 margin-left-5">
        	<div class="col-md-4 bg-transparent-dark padding-all-50 max-width-450"> 
                <div class="internal-text commencement-link-white-hover">
                    <img src="<?php echo SITE_URL; ?>/commencement/img/elephant.png" width="256" height="186" class="img-responsive elephant-head" alt="Silhouette of an elephant head" />
                    <!-- <p>Please join President and Mrs. Bell for a reception at the <a href="https://map.concept3d.com/?id=1222#!m/324486" target="_blank" class="link-white">President's Mansion</a> Friday, December 14.</p> -->
                    <p>The University of Alabama tradition has culminated for UA's Summer 2019 graduates when they participated in the Commencement ceremony on August 3 at <a href="http://bit.ly/2GgemIK" target="_blank" class="link-white">Coleman Coliseum</a>. </p>
                </div> <!-- .internal-text -->
            </div> <!-- .col-md-4 -->
        </div> <!-- .row -->
	</div> <!-- .container --> 
</section>

<script src="https://www.ua.edu/assets/js/froogaloop.js"></script>
<script>
	// Froogaloop orig js
	// https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/froogaloop.js
	
	var iframe = document.getElementById('bgvid');
	
	// $f == Froogaloop
	var player = $f(iframe);
	
	// bind events
	var playButton = document.getElementById("play-button");
	playButton.addEventListener("click", function() {
	  player.api("play");
	}); 
	
	var pauseButton = document.getElementById("pause-button");
	pauseButton.addEventListener("click", function() {
	  player.api("pause");
	  clearTimeout(x); 
	});
</script>

<?php
// Include footer
require_once( ABSPATH . '/commencement/inc/theme/footer.php' );
?>
